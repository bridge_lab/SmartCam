import argparse
import os

import cv2
import matplotlib.pyplot as plt

from super_resolution import SuperResolver, sr_config


def test_model(img_path, resolver):
    img = cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB)

    sr = resolver.do_super_resolution(img)

    fig, (ax0, ax1) = plt.subplots(nrows=1, ncols=2, figsize=(20, 12))

    ax0.imshow(img)
    ax0.set_xticks([])
    ax0.set_yticks([])
    ax0.set_title('Cubic 4X super-resolution')

    ax1.imshow(sr)
    ax1.set_xticks([])
    ax1.set_yticks([])
    ax1.set_title('GAN-based 4X super-resolution')

    file_name = os.path.basename(img_path).split('.')[0]
    out_path = os.path.join(os.path.dirname(img_path), file_name + '_test.png')
    fig.savefig(out_path)

    print('results saved as ', out_path)


def resolve(img_path, resolver):
    img = cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB)

    sr = resolver.do_super_resolution(img)

    file_name = os.path.basename(img_path).split('.')[0]
    out_path = os.path.join(os.path.dirname(img_path), file_name + '_resolved.png')
    cv2.imwrite(out_path, sr)

    print('super-resolved image saved as ', out_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Smart camera CLI program: image super-resolution")
    parser.add_argument('--input_image_path',
                        type=str,
                        help='input image path',
                        default=os.path.normpath(
                            os.path.abspath(
                                os.path.join(
                                    os.path.dirname(
                                        os.path.realpath(__file__)),
                                    'super_resolution',
                                    'tiger.jpeg'))),
                        required=False)
    parser.add_argument('--is_test',
                        type=bool,
                        help='pass True if you want to test the model on a RGB image.'
                             ' If you want inference, pass False.',
                        default=True)

    args = parser.parse_args()
    is_test = args.is_test

    model = SuperResolver(sr_config)

    img_path = args.input_image_path

    if is_test:
        test_model(img_path, model)
    else:
        resolve(img_path, model)