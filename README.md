# Smart Camera

**Smart Camera** project. Let's start by installation.

## Installation

Follow these steps:

1. clone the repository:

`git clone https://gitlab.com/bridge_lab/SmartCam.git`

2. change directory to project's directory:

`cd SmartCam`

3. re-generate the project's conda environment: 

`conda env create -f environment.yml`

4. activate the environment: 

`conda activate SmartCam`


## Colorization

SmartCam colorizes gray images. You can test the colorization on a RGB image.

For testing on a RGB image, do the following from root:

`python colorize.py --input_image_path colorization/tiger.jpeg --is_test True`

If you have a gray-scale image, save it as a three channel image (you can convert it to BGR suing cv2 and save it) and do the following:

`python colorize.py --input_image_path path/to/gray_image.jpeg --is_test False`

![Image colorization](colorization/tiger_test.png "Image colorization")


## Enhancement
For doing image enhancement on a given image, do the following:

`python enhance.py --input_image_path path/to/img.jpg --is_test False`

For testing:

`python enhance.py --input_image_path path/to/image.jpg --is_test True`

![Image enhancement](enhancement/img_test.png "Image enhancement")

## Super Resolution
You can do super-resolution on your image with a 4X factor.

You can test the model:

`python super_resolution.py --input_image_path path/to/image.jpg --is_test True`

Or you can and get the result for your image:

`python super_resolution.py --input_image_path path/to/image.jpg --is_test False`

![Super-Resolution](super_resolution/tiger_test.png "Super-resolution")



## Not Integerated Yet
### Face Swapping
1. Download "shape_predictor_68_face_landmarks.dat" from "https://github.com/AKSHAYUBHAT/TensorFace/tree/master/openface/models/dlib"
2. Run "pip install -r Requirements.txt" from current directory.
3. Run "photos_face_swapping.py"
### PGAN
1. In "PGAN/parsing_network" run "pip install -r requirements.txt"
2. Get model from [link](https://jbox.sjtu.edu.cn/l/hJjgjw) and extract in "PGAN/parsing_network"
3. Put the image in "PGAN/parsing_network" folder with name "1.jpg"
4. Run "PGAN/parsing_network/inference.py"