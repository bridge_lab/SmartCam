import os
import argparse

import cv2
import numpy as np
import matplotlib.pyplot as plt

from colorization import Colorizer, colorizer_config


def test_model(rgb_img_path, model):
    img = cv2.resize(cv2.imread(rgb_img_path), (colorizer_config['input_height'],
                                                colorizer_config['input_width']))
    gray = np.expand_dims(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY), axis=-1) / 255
    input_data = np.expand_dims(gray, axis=0)
    pred = model.colorize(input_data)

    fig, (ax0, ax1, ax2) = plt.subplots(nrows=1, ncols=3, figsize=(16, 8))

    rgb_img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    gray_img = cv2.cvtColor(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY), cv2.COLOR_GRAY2RGB)

    ax0.imshow(rgb_img)
    ax0.set_xticks([])
    ax0.set_yticks([])
    ax0.set_title('GT')

    ax1.imshow(gray_img)
    ax1.set_xticks([])
    ax1.set_yticks([])
    ax1.set_title('Gray image')

    ax2.imshow(pred[0][0])
    ax2.set_xticks([])
    ax2.set_yticks([])
    ax2.set_title('Colorized')

    file_name = os.path.basename(rgb_img_path).split('.')[0]
    out_path = os.path.join(os.path.dirname(rgb_img_path), file_name + '_test.png')
    fig.savefig(out_path)

    print('results saved as ', out_path)


def colorize(gray_img_path, model):

    """Use this for colorizing a gray-scale input image.

    :arg gray_img_path: path to gray-scale image, saved as BGR format with with 3 channels.
    :arg model: An instance of Colorizer model.
    """

    img = cv2.resize(cv2.imread(gray_img_path), (colorizer_config['input_height'],
                                                 colorizer_config['input_width']))
    gray = np.expand_dims(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY), axis=-1) / 255
    input_data = np.expand_dims(gray, axis=0)
    pred = model.colorize(input_data)

    file_name = os.path.basename(gray_img_path).split('.')[0]
    out_path = os.path.join(os.path.dirname(gray_img_path), file_name + '_colorized.png')
    cv2.imwrite(out_path, pred[0][0])

    print('colorized image saved as ', out_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Smart camera CLI program: image colorization")
    parser.add_argument('--input_image_path',
                        type=str,
                        help='input image path',
                        default=os.path.normpath(
                            os.path.abspath(
                                os.path.join(
                                    os.path.dirname(
                                        os.path.realpath(__file__)),
                                    'colorization',
                                    'tiger.jpeg'))),
                        required=False)
    parser.add_argument('--is_test',
                        type=bool,
                        help='pass True if you want to test the model on a RGB image.'
                             ' If you have a gray-scale image for inference, pass False.',
                        default=True)

    args = parser.parse_args()
    is_test = args.is_test

    model = Colorizer(colorizer_config)

    if is_test:
        rgb_img_path = args.input_image_path
        test_model(rgb_img_path, model)
    else:
        gray_img_path = args.input_image_path
        colorize(gray_img_path, model)
